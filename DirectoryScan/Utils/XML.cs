﻿using DirectoryScan.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DirectoryScan.Utils
{
    static class XML
    {
        public static void WriteXML(string fileName, List<DInfo> serializeObject)
        {
            System.Xml.Serialization.XmlSerializer writer =
                new System.Xml.Serialization.XmlSerializer(typeof(List<DInfo>));

            using (System.IO.FileStream file = System.IO.File.Create(fileName))
            {
                writer.Serialize(file, serializeObject);
            }

        }
    }
}
