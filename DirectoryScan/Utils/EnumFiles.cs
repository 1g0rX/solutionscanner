﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DirectoryScan.Utils
{
    static class FileInformation
    {
       public static FileInfo[] Get(DirectoryInfo root)
        {
            FileInfo[] files = null;
            List<FileInfo> fileInfos = new List<FileInfo>();

            try
            {
                files = root.GetFiles();
            }
            catch (UnauthorizedAccessException)
            {
               
            }
            catch (DirectoryNotFoundException)
            {
              
            }

            if (files != null)
            {
                fileInfos.AddRange(files);

                DirectoryInfo[] subDirs = root.GetDirectories();
                foreach (DirectoryInfo dirInfo in subDirs)
                {
                    fileInfos.AddRange(Get(dirInfo));
                }
            }

            return fileInfos.ToArray();
        }

        public static IEnumerable<DirectoryInfo> GetDirectories(DirectoryInfo root)
        {
            IEnumerable<DirectoryInfo> directories = null ;

            try
            {
                directories = root.EnumerateDirectories();
            }
            catch (UnauthorizedAccessException)
            {
                yield break;
            }
            catch (DirectoryNotFoundException)
            {
                yield break;
            }

            foreach (var di in directories)
            {
                //if (di.Name != ".git" && di.Name != ".vs" && di.Name != "obj" && di.Name != "bin")
                yield return di;
            }

            if (!root.HasFileWithExtension(".csproj"))
            {
                DirectoryInfo[] subDirs = root.GetDirectories();
                foreach (DirectoryInfo dirInfo in subDirs)
                {
                    if (dirInfo.Name == ".git" || dirInfo.Name == ".vs") continue;

                    foreach (var di in GetDirectories(dirInfo))
                    {
                        yield return di;
                    }
                }
            }

        }

        public static IEnumerable<FileInfo> GetEnum(DirectoryInfo root)
        {
            IEnumerable<FileInfo> files;
            try
            {
                files = root.EnumerateFiles();
            }
            catch (UnauthorizedAccessException)
            {
                yield break;
            }
            catch (DirectoryNotFoundException)
            {
                yield break;
            }

            if (files != null)
            {
                foreach (var fi in files)
                {
                    yield return fi;
                }

                DirectoryInfo[] subDirs = root.GetDirectories();
                foreach (DirectoryInfo dirInfo in subDirs)
                {
                    foreach (var fi in GetEnum(dirInfo))
                    {
                        yield return fi;
                    }
                }
            }
        }

        public static bool HasFileWithExtension(this DirectoryInfo dir, string ext)
        {
            var result = dir.EnumerateFiles().FirstOrDefault(f => f.Extension == ext);
            return result == null ? false : true;
        }
    }
}
