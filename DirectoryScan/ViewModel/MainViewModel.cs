﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections.Concurrent;
using Utils;
using DirectoryScan.Model;
using System.Collections.ObjectModel;
using System.Windows.Threading;
using System.Windows.Data;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Collections;
using DirectoryScan.Utils;

namespace DirectoryScan.ViewModel
{
    class MainViewModel : PropertyChangedBase, IDataErrorInfo
    {
        #region Properties
        //Запущен ли процесс подсчёта
        private bool isStartCalc;
        public bool IsStartCalc
        {
            get { return isStartCalc; }
            set { SetField(ref isStartCalc, value); }
        }

        //Текущий обрабатываемый файл
        private string currentFile;
        public string CurrentFile
        {
            get { return currentFile; }
            set { SetField(ref currentFile, value); }
        }

        //Размер папки
        private long directorySize;
        public long DirectorySize
        {
            get { return directorySize; }
            set { SetField(ref directorySize, value); }
        }

        //Путь к начальной папке
        private string directoryPath = null;
        public string DirectoryPath
        {
            get { return directoryPath; }
            set
            {
                SetField(ref directoryPath, value);
            }
        }

        //Список файлов и их размер в байтах
        private ObservableCollection<DInfo> fInfos = new ObservableCollection<DInfo>();
        public ObservableCollection<DInfo> DInfos
        {
            get { return fInfos; }
            set { SetField(ref fInfos, value); }
        }
        #endregion

        #region Fields
        CancellationTokenSource cancelTokenSource = new CancellationTokenSource();
        private readonly object _collectionOfObjectsSync = new object();
        #endregion

        public MainViewModel()
        {
            Dispatcher.CurrentDispatcher.BeginInvoke(new Action(() =>
            { BindingOperations.EnableCollectionSynchronization(fInfos, _collectionOfObjectsSync); }));
         
        }

        #region Commands
        //Реализует диалог выбора файла
        private RelayCommand openDirDialog;
        public RelayCommand OpenDirDialog =>
            openDirDialog ?? (openDirDialog = new RelayCommand(obj =>
                {
                    FolderBrowserDialog dialog = new FolderBrowserDialog();
                    dialog.Description = "Выберите папку";
                    dialog.SelectedPath = DirectoryPath;
                    DialogResult result = dialog.ShowDialog();
                    if (result == DialogResult.OK) DirectoryPath = dialog.SelectedPath;
                }));

        //Подсчёт размера папки
        private RelayCommand calcDirectory;
        public RelayCommand CalcDirectory =>
            calcDirectory ?? (calcDirectory = new RelayCommand(async obj =>
            {
                if (string.IsNullOrWhiteSpace(DirectoryPath)) return;
                //Если подсчёт запущен - прерываем
                if (isStartCalc)
                {
                    cancelTokenSource.Cancel();
                    return;
                }
                //Инициализация начальных значений переменных
                cancelTokenSource = new CancellationTokenSource();
                IsStartCalc = true;
                DirectorySize = 0;
                CurrentFile = String.Empty;
                DInfos.Clear();

                CancellationToken token = cancelTokenSource.Token;              
                await Task.Factory.StartNew(() => CDir(DirectoryPath, token));

                IsStartCalc = false;
            }));

        private RelayCommand saveFile;
        public RelayCommand SaveFile =>
            saveFile ?? (saveFile = new RelayCommand(async obj =>
            {
                if (DInfos.Count == 0) return;

                XML.WriteXML("ExportFile.xml", DInfos.ToList());
            }));
        #endregion

        #region Validation
        public string Error => null;

        public string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "DirectoryPath":
                        if (string.IsNullOrWhiteSpace(directoryPath))
                            return "Путь не может иметь пустое значение";
                        break;
                }
                return string.Empty;
            }
        }
        #endregion

        #region Methods
        private void CDir(string path, CancellationToken token)
        {
            DirectoryInfo dirInfo = new DirectoryInfo(path);
            Utils.FileInformation.GetDirectories(dirInfo);

            var directories = Utils.FileInformation.GetDirectories(dirInfo); 

            if (directories is null) return;

            foreach (var di in directories)
            {
                if (token.IsCancellationRequested) break;

                lock (_collectionOfObjectsSync)
                {
                    bool hasCSProjFile = di.HasFileWithExtension(".csproj");
                    bool hasSlnFile = di.HasFileWithExtension(".sln");

                    SolutionFolderType folderType = SolutionFolderType.Folder;

                    if (hasCSProjFile || hasSlnFile)
                    {
                        folderType = hasCSProjFile ? SolutionFolderType.Project : SolutionFolderType.Solution;
                    }

                    CurrentFile = di.FullName;
                    DInfos.Add(new DInfo
                    {
                        Path = di.FullName,
                        Name = di.Name,
                        Type = folderType,
                        Description = string.Empty,
                        SolutionName = string.Empty,
                        ValidateDate = DateTime.Today,
                        ParentDir = di.Parent.Name }); ; ;

               
                    //DirectorySize += di.size;
                }
            }
        }
        #endregion

    }
}

