﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DirectoryScan.Model
{
    public enum SolutionFolderType
    {
        Folder,
        Project,
        Solution
    }

    [Serializable]
    public class DInfo
    {
        public string ParentDir { get; set; }
        public string Name { get; set; }
        public SolutionFolderType Type { get; set; }
        public string Description { get; set; }

        public string SolutionName { get; set; }
        public string Path { get; set; }
        public DateTime ValidateDate { get; set; }

    }
}
